/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月16日 下午9:22:55  created
 */
package com.desktop.web.config;

/**
 * 
 *
 * @author baibai
 */
public class ErrorCode {

    /**
     * 重新授权
     */
    public static final int RESET_AUTH = 1;

    /**
     * 重新登录
     */
    public static final int RESET_LOGIN = 2;

}
