/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform baibai. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of baibai.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with baibai.
 * 
 * Modified history:
 *   baibai  2020年4月14日 下午9:40:47  created
 */
package com.desktop.web.core.comenum;

/**
 * 
 *
 * @author baibai
 */
public enum RemoteColorType {
    COLOR8(8), COLOR16(16), COLOR32(32);

    private int color;

    private RemoteColorType(int color) {
        this.color = color;
    }

    /**
     * @return the color
     */
    public int getColor() {
        return color;
    }

    /**
     * @param color the color to set
     */
    public void setColor(int color) {
        this.color = color;
    }

}
