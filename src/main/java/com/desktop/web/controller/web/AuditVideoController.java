/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月2日 下午11:58:03  created
 */
package com.desktop.web.controller.web;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.desktop.web.core.aop.RightTarget;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.core.web.Result;
import com.desktop.web.service.auditvideo.AuditVideoService;
import com.desktop.web.uda.entity.AuditVideo;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Controller
@RequestMapping("/webapi/auditvideo")
public class AuditVideoController extends BaseWebController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AuditVideoService auditVideoService;

    @RequestMapping(value = "/list/get", method = {RequestMethod.GET})
    @ResponseBody
    @RightTarget(value = "browser_audit_video_list")
    public Object getList(HttpServletRequest request) {

        try {

            String ip = request.getParameter("ip");
            String username = request.getParameter("username");
            String deviceName = request.getParameter("deviceName");
            String stime = request.getParameter("stime");
            String etime = request.getParameter("etime");

            int size = Integer.valueOf(request.getParameter("size"));
            int page = Integer.valueOf(request.getParameter("offset")) / size + 1;

            IPage<AuditVideo> pageInfo = auditVideoService.getAuditVideoList(ip, username, deviceName, stime, etime, page, size);
            List<Map<String, Object>> retList = new ArrayList<Map<String, Object>>();
            for (AuditVideo item : pageInfo.getRecords()) {
                Map<String, Object> tempitem = new HashMap<String, Object>();
                tempitem.put("ctime", item.getCtime());
                tempitem.put("deviceName", item.getDeviceName());
                tempitem.put("username", item.getUsername());
                tempitem.put("uuid", item.getPath());
                tempitem.put("id", item.getId());
                tempitem.put("ip", item.getIp());
                retList.add(tempitem);
            }

            return Result.Success(retList);
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/video/del", method = {RequestMethod.POST})
    @ResponseBody
    @RightTarget(value = "browser_audit_video_manger")
    public Object delVideo(HttpServletRequest request) {

        try {
            Map<String, Object> params = RequestUtil.getBody2(request);
            if (params == null || params.isEmpty()) {
                return null;
            }

            User user = this.getCurUser();
            Long id = Long.valueOf(params.get("id").toString());

            auditVideoService.delAuditVideoByIdTargetid(user, id, RequestUtil.getUid());
            return new Result();
        } catch (BusinessException e) {
            logger.error(e.getMessage(), e);
            return Result.Error(e.getMessage());
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return Result.Error();
        }
    }

    @RequestMapping(value = "/file/get", method = {RequestMethod.GET})
    @RightTarget(value = "browser_audit_video_list")
    public void getFile(HttpServletRequest request, HttpServletResponse response) {

        try {
            String uuid = request.getParameter("uuid");
            File file = auditVideoService.getFileByuuidCuruser(uuid);

            if (file == null) {
                logger.error("not find file info by uuid:{}", uuid);
                return;
            }

            response.setContentType("application/force-download");
            response.addHeader("Content-Length", file.length() + "");
            response.addHeader("Content-Disposition", "attachment;fileName=" + java.net.URLEncoder.encode(file.getName(), "UTF-8"));
            byte[] buffer = new byte[1024];
            FileInputStream fis = null;
            BufferedInputStream bis = null;
            try {
                fis = new FileInputStream(file);
                bis = new BufferedInputStream(fis);
                OutputStream os = response.getOutputStream();
                int i = bis.read(buffer);
                while (i != -1) {
                    os.write(buffer, 0, i);
                    i = bis.read(buffer);
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            } finally {
                if (bis != null) {
                    try {
                        bis.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}
