/**
 * $Revision$
 * $Date$
 *
 * Copyright (C) https://gitee.com/baibaiclouds/platform loon. All rights reserved.
 * <p>
 * This software is the confidential and proprietary information of loon.
 * You shall not disclose such Confidential Information and shall use it only
 * in accordance with the terms of the agreements you entered into with loon.
 * 
 * Modified history:
 *   Loon  2019年11月19日 下午11:04:04  created
 */
package com.desktop.web.service.remotecpe;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.desktop.web.core.comenum.RemoteProtocolType;
import com.desktop.web.core.comenum.Status;
import com.desktop.web.core.exception.BusinessException;
import com.desktop.web.core.utils.RequestUtil;
import com.desktop.web.service.ack.ACKService;
import com.desktop.web.service.ack.CompletableFutureInfo;
import com.desktop.web.service.device.DeviceService;
import com.desktop.web.service.session.DeviceSessionService;
import com.desktop.web.service.session.Message;
import com.desktop.web.service.user.UserService;
import com.desktop.web.uda.entity.Device;
import com.desktop.web.uda.entity.DeviceSettings;
import com.desktop.web.uda.entity.User;

/**
 * 
 *
 * @author baibai
 */
@Service
public class RemotecpeService {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${remotedevice.shutdown.seconds}")
    private Integer SHOWDOWN_DEVICE_SECONDS;

    @Autowired
    private DeviceSessionService sessionService;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private ACKService ackService;

    @Autowired
    private NatService natService;

    @Autowired
    private UserService userService;

    @Autowired
    private VirtulNatService virtulNatService;

    /**
     * 关闭远程连接
     * 
     * @param natInfo
     */
    public void closeRemoteControl(NatInfo natInfo) {
        Message msg = new Message();
        msg.setEvent(Message.Type.CLOSE_REMOTE_CONTROL);
        sessionService.sendMsg(natInfo.getDeviceId(), msg);
    }

    /**
     * 开始远程控制 type is vnc
     * 
     * @param user
     * @param deviceId
     */
    public NatInfo startRemoteControl(User user, Map<String, String> params) {

        Long deviceId = Long.valueOf(params.get("deviceId"));
        Device device = deviceService.getDeviceById(deviceId);
        if (device == null) {
            throw new BusinessException("找不到设备");
        }

        deviceService.checkDeviceByCuruser(deviceId);
        if (device.getIsprober() == Status.NO) {
            return startRemoteNoproberControl(device, params);
        }

        boolean isonline = sessionService.deviceIsOnline(deviceId);
        if (!isonline) {
            throw new BusinessException("设备已离线，无法发起远程");
        }

        if (natService.isRemoteingByDeviceSN(device.getSn())) {
            throw new BusinessException("设备正在远程控制中，请稍后再试");
        }

        RemoteProtocolType protocolType = RemoteProtocolType.valueOf(params.get("type").toUpperCase());
        NatInfo natInfo = natService.makeNatInfo(user, device, protocolType, params);
        if (natInfo == null) {
            throw new BusinessException("系统资源不足，请稍后");
        }

        DeviceSettings deviceSettings = deviceService.getDeviceSettings(device.getSn());
        CompletableFutureInfo tempCompletableFutureInfo = ackService.make(user);
        natInfo.setAckuuid(tempCompletableFutureInfo.getUuid());
        natInfo.setRemoteack(deviceSettings.getAutoRemote());

        Message msg = new Message();
        msg.setEvent(Message.Type.START_REMOTE_CONTROL);
        msg.setBody(natInfo);
        sessionService.sendMsg(deviceId, msg);

        logger.info("user start remote control success,uid:{},did:{}", user.getId(), deviceId);
        String acktype = null;
        try {
            acktype = tempCompletableFutureInfo.get("请求超时，请检查网络是否正常").toString();
        } catch (Exception e) {
            natService.gcPortByUUID(natInfo.getUuid());
            throw new BusinessException(e.getMessage());
        }

        if (acktype.equals("no")) {
            throw new BusinessException("远程机器拒绝了你的请求");
        }

        return natInfo;
    }

    /**
     * 软设备远程控制
     * 
     * @param device
     * @return
     */
    private NatInfo startRemoteNoproberControl(Device device, Map<String, String> params) {
        User user = userService.getUserById(RequestUtil.getUid());
        NatInfo natInfo = virtulNatService.makeNatInfo(user, device, params);
        if (natInfo == null) {
            throw new BusinessException("远程控制出错，请联系管理员");
        }
        return natInfo;
    }

    /**
     * 关闭计算机
     * 
     * @param user
     * @param deviceId
     */
    public void shutdownDevice(User user, Long deviceId) {

        deviceService.checkDeviceByCuruser(deviceId);

        boolean isonline = sessionService.deviceIsOnline(deviceId);
        if (!isonline) {
            throw new BusinessException("设备已离线，无法发起关机");
        }

        Device device = deviceService.getDeviceById(deviceId);
        if (device == null) {
            throw new BusinessException("找不到设备");
        }

        Message msg = new Message();
        msg.setEvent(Message.Type.SHUTDOWN);
        Map<String, Object> body = new HashMap<>();
        body.put("seconds", SHOWDOWN_DEVICE_SECONDS);
        msg.setBody(body);

        sessionService.sendMsg(deviceId, msg);
        logger.info("user shutdown success,uid:{},did:{}", user.getId(), deviceId);
    }

    /**
     * 获取nat信息
     * 
     * @param uuid
     * @return
     */
    public NatInfo getNatInfoByUUID(String uuid) {
        if (natService.existUUID(uuid)) {
            return natService.getPortInfoByUUID(uuid);
        } else {
            return virtulNatService.getNatInfoByUUID(uuid);
        }
    }

    /**
     * ping
     * 
     * @param uuid
     */
    public NatInfo ping(String uuid) {
        NatInfo natInfo = getNatInfoByUUID(uuid);
        if (natInfo == null) {
            return null;
        }

        natInfo.setLastDate(new Date());
        return natInfo;
    }
}
