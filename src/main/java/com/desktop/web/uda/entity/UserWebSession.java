/**
 * 
 */
package com.desktop.web.uda.entity;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.desktop.web.core.db.BaseEntity;

/**
 * @author baibai
 *
 */
@TableName("t_user_web_session")
public class UserWebSession extends BaseEntity {

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    private Long uid;
    private String token;
    private Date ctime;

    /**
     * @return the uid
     */
    public Long getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(Long uid) {
        this.uid = uid;
    }

    /**
     * @return the token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the token to set
     */
    public void setToken(String token) {
        this.token = token;
    }

    /**
     * @return the ctime
     */
    public Date getCtime() {
        return ctime;
    }

    /**
     * @param ctime the ctime to set
     */
    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

}
